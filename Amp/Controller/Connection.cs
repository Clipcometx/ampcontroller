﻿using System.Net;
using System.Net.Sockets;

namespace Amp.Controller
{
    internal class Connection
    {
        private static string server = "127.0.0.1";
        private static int serverPort = 3456;
        private Socket soc = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        private static IPAddress ipAdd = System.Net.IPAddress.Parse(server);
        private IPEndPoint remoteEP = new IPEndPoint(ipAdd, serverPort);

        public void connect(string _server,int _port)
        {
            server = _server;
            serverPort = _port;
        }

        public void Send(string command)
        {
            //Start sending stuf..
            byte[] byData = System.Text.Encoding.ASCII.GetBytes(command);
            soc.Send(byData);
        }

        public string Recive()
        {
            soc.Connect(remoteEP);
            byte[] buffer = new byte[1024];
            int iRx = soc.Receive(buffer);
            char[] chars = new char[iRx];

            System.Text.Decoder d = System.Text.Encoding.UTF8.GetDecoder();
            int charLen = d.GetChars(buffer, 0, iRx, chars, 0);
            System.String recv = new System.String(chars);
            soc.Disconnect(false);
            soc.Close();

            return recv;

        }
    }
}